# mw_connected

A small mediawiki extension to have a variable to know if the user is connected or not.

    Version 0.3
    Mathieu Loiseau

This is free software licensed under the GNU General Public License. Please
see http://www.gnu.org/copyleft/gpl.html for further details, including the
full text and terms of the license.

## Overview

Connected is an extension to MediaWiki that provides elements to adapt the content of a page to the user (or lack thereof).

The extension defines the following parser functions and variables:
* ``LOGGEDIN`` : ``1`` if the user is logged in, ``0`` otherwise ;
* ``USERNAME`` : the user's screen name, if they are logged in, their ip address otherwise ;
* ``#ifloggedin`` : displays the first parameter if the user is logged in, the second otherwise.

The extension hides the sidebar for not logged in users.

It also provides two css depending on the status of the user :
* ``MediaWiki:NotLoggedIn.css`` (which should be listed in ``$wgWhitelistRead``)
* ``MediaWiki:LoggedIn.css``

## Installation
To install the extension, place the entire 'Connected' directory within
your MediaWiki `extensions` directory, then add the following line to
your `LocalSettings.php` file:
```php
    wfLoadExtension( 'Connected' );
```
