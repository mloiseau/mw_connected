<?php
$magicWords = [];

$magicWords['en']  = [
	'connected'		=> [1, 'LOGGEDIN'],
	'u_name'		=> [1, 'USERNAME'],
	'iflogged'		=> [0, 'ifloggedin']
];

$magicWords['fr']  = [
	'connected'		=> [1, 'CONNECTE'],
	'u_name'		=> [1, 'NOM'],
	'iflogged'		=> [0, 'siconnecte']
];
