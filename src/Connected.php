<?php
/**
 * Connected
 *
 * @license		GPL
 * @package		Connected
 * @link		https://gitlab.liris.cnrs.fr/mloiseau/mw_connected
 *
 **/

class ConnectedHooks {
	private static $user = false;

	// Register any render callbacks with the parser
	public static function onParserFirstCallInit( Parser $parser ) {
		// Create a function hook associating the "example" magic word with renderExample()
		$parser->setFunctionHook( 'iflogged', [ self::class, 'ifConnectedRender' ] );
	}

	//try to get the user anyway I can
	//through user or parser or output or wgUser…
	//and set it for next time
	private static function get_user($parserOrOther=null){
		if (self::$user === false){
			self::$user = RequestContext::getMain()->getUser();
		}
		return self::$user;
	}

	private static function is_registered(){
		return self::$user != false && self::$user->mId !=0;
	}

	// Render the output of {{#ifloggedin:}} i.e. iflogged function.
	public static function ifConnectedRender( Parser $parser, $ifLoggedIn = '', $ifNot = '') {
		// The input parameters are wikitext with templates expanded.
		// The output should be wikitext too.
		$parser->getOutput()->updateCacheExpiry( 0 );
		self::get_user($parser);

		if (self::is_registered()) {
			$output = $ifLoggedIn;
		}
		else{
			$output = $ifNot;
		}
		return [$output, 'noparse' => false ];
	}

	//calcul des variables
	public static function wfConnectedAssignValue( Parser $parser, &$cache, $magicWordId, &$ret, $frame) {
		$result = false;
		self::get_user();
		$parser->getOutput()->updateCacheExpiry( 0 );
		switch($magicWordId){
			case 'connected' :
				if (!self::is_registered()) {
					$ret = 0;
				}
				else{
					$ret = 1;
				}
				$result = true;
				break;
			case 'u_name':
				$ret = self::$user->mName;
				$result = true;
				break;
		}
		return $result;
	}

	//mettre à jour la liste de variables
	public static function connectedDeclareVarIds( &$customVariableIds ) {
		// $customVariableIds is where MediaWiki wants to store its list of custom
		// variable IDs. We oblige by adding ours:
		$customVariableIds[] = 'connected';
		$customVariableIds[] = 'u_name';
	}

	#Cacher la barre de menu quand on n'est pas logué et charger une css supplémentaire
//https://www.mediawiki.org/wiki/Manual:Interface/Sidebar#Change_sidebar_content_when_logged_in_(PHP)
//https://www.mediawiki.org/wiki/Snippets/Load_JS_and_CSS_by_URL
	public static function lfHideSidebar( $skin, &$bar ) {
		// Hide sidebar for anonymous users
		$user = self::get_user();
		if (!self::is_registered()) {
			$bar = array(
				'navigation' => array(
					array(
						'text'   => wfMessage( 'login' ) -> text(),
						'href'   => SpecialPage::getTitleFor( 'Login' )->getLocalURL(),
						'id'     => 'n-login',
						'active' => ''
					)
				)
			);
		}
		return true;
	}

	#Ajouter une feuille de style quand pas connecté
	public static function add_NotLoggedIncss( OutputPage $out, Skin $skin ) {
		$user = self::get_user($out);
		if (!self::is_registered()) {
			$out->addStyle('/w/index.php?title=MediaWiki:NotLoggedIn.css&action=raw&ctype=text/css');
			$out->addMeta( "logged", "no" );
		}
		else{
			$out->addStyle('/w/index.php?title=MediaWiki:LoggedIn.css&action=raw&ctype=text/css');
			$out->addMeta( "logged", "yes" );
		}
	}

	// *** use a specific cache hash key for registered users
	// so the cache of a page is always related to anonymous/registered_users
	//https://github.com/thomas-topway-it/mediawiki-extensions-PageOwnership/blob/13c379e0bac178a963d08a63fd776a30fa85a339/includes/PageOwnership.php#L326
	public static function onPageRenderingHash( &$confstr, User $user, &$forOptions ) {
		// *** for some reason we cannot rely on $user->isRegistered()
		if ( $user->isRegistered() ) {
			$confstr .= '+registered_user';
		}

	}
}
